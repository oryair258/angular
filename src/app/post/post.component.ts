import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post';

@Component({
  selector: 'or-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
post:Post;

  title1;
  body1;
  isEdit:Boolean = false;
  editButtonText='Edit';

@Output() editEvent = new EventEmitter<Post>();
@Output() deleteEvent = new EventEmitter<Post>();


  sendDelete(){
	  this.deleteEvent.emit(this.post);
	  }


	  toggleEdit(){
	    this.isEdit=!this.isEdit;
	    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
      this.isEdit ? this.saveOldProperties() : this.saveNewProperties() ;
      console.log(this.isEdit);
  }

  calcelChanges(){
    this.post.title = this.title1;
    this.post.body = this.body1;
    this.saveNewProperties();
    this.isEdit=!this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
  }

  saveNewProperties(){
    this.editEvent.emit(this.post);
  }

  saveOldProperties(){
   this.title1=this.post.title;
    this.body1=this.post.body;
  }


  constructor() { }
  

  ngOnInit() {
  }

}