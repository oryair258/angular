import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import {RouterModule,Routes} from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';

const appRoutes:Routes = [
{path:'users', component: UsersComponent},
{path:'posts', component: PostsComponent},
{path:'', component: PostsComponent},
{path:'**', component: PageNotFoundComponent},
]

export const firebaseconfig = {
    apiKey: "AIzaSyDNZwENEuaAD7Rnl6-EUu9kd-J7DGVbkYg",
    authDomain: "angular-home-611cc.firebaseapp.com",
    databaseURL: "https://angular-home-611cc.firebaseio.com",
    storageBucket: "angular-home-611cc.appspot.com",
    messagingSenderId: "590890151962"
  }
 
@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
	  RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseconfig)

  ],
  providers: [PostsService,UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
