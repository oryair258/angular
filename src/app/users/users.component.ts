import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'or-users',
  templateUrl: './users.component.html',
  //styleUrls: ['./user.component.css'],
  styles: [`
    users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #ecf0f1;
} 
  `]
})
export class UsersComponent implements OnInit {

  users;
 isLoading:Boolean = true;
 currentUser;

constructor(private _userService: UsersService) {}

 select(user){
		this.currentUser = user; 
    console.log(this.currentUser);
 }

 addUser(user){
   this._userService.addUsers(user);
 }
  
  deleteUser(user){
    this.users.splice(this.users.indexOf(user),1)
  }
  
  updateUser(user)
  {
    this._userService.updateUser(user);
  }
  

  ngOnInit() {
        this._userService.getUsers().subscribe(usersData =>{this.users=usersData; this.isLoading=false});
  }

}