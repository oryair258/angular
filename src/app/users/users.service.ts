import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import { AngularFire } from 'angularfire2';


@Injectable()
export class UsersService {
usersObservable;
//private _url='http://jsonplaceholder.typicode.com/users';
 
  getUsers(){ 
    //return this._http.get(this._url).map(res =>res.json()).delay(2000);
    this.usersObservable = this._af.database.list('/users') ;
    return this.usersObservable;
    }

    addUsers(user){
      this.usersObservable.push(user);
    }

    updateUser(user){
      let userKey = user.$key;
      let userData = {name:user.name,email:user.email};
      this._af.database.object('/users/' + userKey).update(userData);
    }

    deleteUser(user)
    {
      this._af.database.object('/users/' + user.$key).remove();
    }
  constructor(private _af:AngularFire) { }

}