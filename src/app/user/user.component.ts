import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {User} from './user';

@Component({
  selector: 'or-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']

})
export class UserComponent implements OnInit {

  user:User; 
isEdit:Boolean = false;
editButtonText = 'Edit';

@Output()deleteEvant = new EventEmitter<User>();
 
constructor() { }

sendDelete(){
    this.deleteEvant.emit(this.user);
}
  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit' 
  }
 
   ngOnInit() {   }
}