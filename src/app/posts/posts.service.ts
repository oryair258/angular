import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';


@Injectable()
export class PostsService {
    //private _url='http://jsonplaceholder.typicode.com/posts'; 
  postObservable;

    getPosts(){
    this.postObservable = this.af.database.list('/posts').map(
    posts =>{
      posts.map(
        post => {
          post.userName =[];
          for(var u in post.users){
            post.userName.push(
              this.af.database.object('/users/' + u)
            )
          } }
      );
      return posts;}
      );
  return this.postObservable;
  }

    addPost(post){
    this.postObservable.push(post);
    }

    updatePost(post){
      let postKey = post.$key;
      let postData = {title: post.title, body: post.body}
      this.af.database.object('/posts/'+ postKey).update(postData);
    }

    deletePost(post){
      let postKey = post.$key;
      this.af.database.object('/posts/'+ postKey).remove();
    }


 constructor(private af:AngularFire) { }  
}