import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';


@Component({
  selector: 'or-posts',
  templateUrl: './posts.component.html',
  //styleUrls: ['./posts.component.css']
   styles: [`
   .posts li { cursor: default; } 
   .posts li:hover { background: #ecf0f1; }  
   .list-group-item.active,
   .list-group-item.active:hover { 
     background-color: #ecf0f1;
     border-color: #ecf0f1; 
      color: #2c3e50;}
  `]
})

export class PostsComponent implements OnInit {
  posts;
  currentPost;
  isLoading=true;

  constructor(private _postsService:PostsService) { } 

  select(post){
		this.currentPost = post; 
    console.log(this.currentPost);
 }

 editPost(post){
  this._postsService.updatePost(post);
 }

  addPost(post){
  this._postsService.addPost(post);  }

  

  deleteUser(post){
    this._postsService.deletePost(post);
  }


  ngOnInit() {
    // this.posts = this._postService.getPosts();
     this._postsService.getPosts().subscribe(postsData => {this.posts=postsData; this.isLoading=false});
  }

}